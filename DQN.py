from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.core import Dense, Dropout
import random
import numpy as np
import pandas as pd

# Deep Q Network

class DQNAgent(object):

    def __init__(self, learn):
        self.reward = 0
        self.gamma = 0.9
        self.dataframe = pd.DataFrame()
        self.short_memory = np.array([])
        self.agent_target = 1
        self.agent_predict = 0
        self.learning_rate = 0.0005
        if learn:
            self.model = self.network()
        else:
            self.model = self.network("weights.hdf5")
        self.epsilon = 0
        self.actual = []
        self.memory = []


    def get_state(self, snake, food):
        state = [snake.isStraightDangerous(),  # danger straight
            snake.isRightDangerous(),  # danger right
            snake.isLeftDangerous(),  # danger left
            snake.isMovingLeft(),  # move left
            snake.isMovingRight(),  # move right
            snake.isMovingUp(),  # move up
            snake.isMovingDown(),  # move down
            snake.isFoodLeft(food),  # food left
            snake.isFoodRight(food),  # food right
            snake.isFoodUp(food),  # food up
            snake.isFoodDown(food) # food down
        ]

        for i in range(len(state)):
            if state[i]:
                state[i] = 1
            else:
                state[i] = 0

        return np.asarray(state)

    def give_reward(self):
        return self.set_reward(10)

    def give_punishment(self):
        return self.set_reward(-10)

    def set_reward(self, reward):
        self.reward = reward
        return reward

    def network(self, weights=None):
        model = Sequential()
        model.add(Dense(units=120, activation='relu', input_dim=11))
        model.add(Dropout(0.15))
        model.add(Dense(units=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(units=120, activation='relu'))
        model.add(Dropout(0.15))
        model.add(Dense(units=3, activation='softmax'))
        opt = Adam(self.learning_rate)
        model.compile(loss='mse', optimizer=opt)

        if weights:
            model.load_weights(weights)
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def replay_new(self, memory):
        if len(memory) > 1000:
            minibatch = random.sample(memory, 1000)
        else:
            minibatch = memory
        for state, action, reward, next_state, done in minibatch:
            target = reward
            if not done:
                target = reward + self.gamma * np.amax(self.model.predict(np.array([next_state]))[0])
            target_f = self.model.predict(np.array([state]))
            target_f[0][np.argmax(action)] = target
            self.model.fit(np.array([state]), target_f, epochs=1, verbose=0)

    def train_short_memory(self, state, action, reward, next_state, done):
        target = reward
        if not done:
            target = reward + self.gamma * np.amax(self.model.predict(next_state.reshape((1, 11)))[0])
        target_f = self.model.predict(state.reshape((1, 11)))
        target_f[0][np.argmax(action)] = target
        self.model.fit(state.reshape((1, 11)), target_f, epochs=1, verbose=0)
