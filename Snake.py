import pygame
import random
from enum import Enum
from DQN import DQNAgent
from keras.utils import to_categorical
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from random import randint

class Direction(Enum):
    RIGHT = 1,
    DOWN = 2,
    LEFT = 3,
    UP = 4

class Cube(object):

    def __init__(self, start, dirnx = 1, dirny = 0, color = (255, 0, 0)):
        self.pos = start
        self.dirnx = 0
        self.dirny = 0
        self.color = color

    def move(self, dirnx, dirny):
        self.dirnx = dirnx
        self.dirny = dirny
        self.pos = (self.pos[0] + self.dirnx, self.pos[1] + self.dirny)

    def draw(self, surface, eyes = False):
        global rows, width
        dis = width // rows
        i = self.pos[0]
        j = self.pos[1]

        pygame.draw.rect(surface, self.color, (i * dis + 1 , j * dis +1, dis - 1, dis - 1))

        if eyes:
            centre = dis // 2
            radius = 3
            circleMiddle = (i*dis+centre-radius, j*dis+8)
            circleMiddle2 = (i*dis+centre+radius, j*dis+8)

            pygame.draw.circle(surface, (0, 0, 0), circleMiddle, radius)
            pygame.draw.circle(surface, (0, 0, 0), circleMiddle2, radius)

class Snake(object):
    body = []
    turns = {}

    def __init__(self, color, pos):
        self.color = color
        self.head = Cube(pos)
        self.body.append(self.head)
        self.dirnx = 0
        self.dirny = 0
        self.movedir = None

    def moveAgent(self, move):
        if np.array_equal(move, [1, 0, 0]):
            self.moveInRelativeDirection(Direction.LEFT)
        if np.array_equal(move, [0, 0, 1]):
            self.moveInRelativeDirection(Direction.RIGHT)

    def moveInRelativeDirection(self, dir):
        if dir == Direction.LEFT:
            if self.movedir is None:
                self.moveInDirection(Direction.LEFT)
                return

            if self.movedir == Direction.LEFT:
                self.moveInDirection(Direction.DOWN)
                return

            if self.movedir == Direction.RIGHT:
                self.moveInDirection(Direction.UP)
                return

            if self.movedir == Direction.UP:
                self.moveInDirection(Direction.LEFT)
                return

            if self.movedir == Direction.DOWN:
                self.moveInDirection(Direction.RIGHT)
                return

        if dir == Direction.RIGHT:
            if self.movedir is None:
                self.moveInDirection(Direction.RIGHT)
                return

            if self.movedir == Direction.LEFT:
                self.moveInDirection(Direction.UP)
                return

            if self.movedir == Direction.RIGHT:
                self.moveInDirection(Direction.DOWN)
                return

            if self.movedir == Direction.UP:
                self.moveInDirection(Direction.RIGHT)
                return

            if self.movedir == Direction.DOWN:
                self.moveInDirection(Direction.LEFT)
                return


    def moveInDirection(self, dir):
        if dir == Direction.LEFT:
            self.movedir = Direction.LEFT
            if self.dirnx == 0:
                self.dirnx = -1
                self.dirny = 0
                self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        if dir == Direction.RIGHT:
            self.movedir = Direction.RIGHT
            if self.dirnx == 0:
                self.dirnx = 1
                self.dirny = 0
                self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        if dir == Direction.UP:
            self.movedir = Direction.UP
            if self.dirny == 0:
                self.dirnx = 0
                self.dirny = -1
                self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

        if dir == Direction.DOWN:
            self.movedir = Direction.DOWN
            if self.dirny == 0:
                self.dirnx = 0
                self.dirny = 1
                self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]


    def movePlayer(self):
        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:

            self.moveInDirection(Direction.LEFT)

        elif keys[pygame.K_RIGHT]:

            self.moveInDirection(Direction.RIGHT)

        elif keys[pygame.K_UP]:

            self.moveInDirection(Direction.UP)

        elif keys[pygame.K_DOWN]:

            self.moveInDirection(Direction.DOWN)

    def move(self):
        global s, rows, gameOver, gameCounter

        for i,c in enumerate(self.body):
            p = c.pos[:]

            if p in self.turns:
                turn = self.turns[p]
                if turn[0] == -1 and c.pos[0] <= 0:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit left wall!"
                    crash()
                    break
                elif turn[0] == 1 and c.pos[0] >= rows - 1:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit right wall!"
                    crash()
                    break
                elif turn[1] == -1 and c.pos[1] <= 0:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit upper wall!"
                    crash()
                    break
                elif turn[1] == 1 and c.pos[1] >= rows - 1:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit bottom wall!"
                    crash()
                    break
                else:
                    c.move(turn[0], turn[1])
                    if i == len(self.body) - 1:
                        self.turns.pop(p)
            else:
                if c.dirnx == -1 and c.pos[0] <= 0:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit left wall!"
                    crash()
                    break
                elif c.dirnx == 1 and c.pos[0] >= rows - 1:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit right wall!"
                    crash()
                    break
                elif c.dirny == -1 and c.pos[1] <= 0:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit upper wall!"
                    crash()
                    break
                elif c.dirny == 1 and c.pos[1] >= rows - 1:
                    gameOver = "Round " + str(gameCounter) + " over: Snake hit bottom wall!"
                    crash()
                    break
                else:
                    c.move(c.dirnx, c.dirny)

    def reset(self, pos):
        global score, lastScore, highScore, scores

        scores.append(score)

        lastScore = score

        if lastScore > highScore:
            highScore = lastScore

        score = 0

        self.head = Cube(pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 0

    def addCube(self):
        tail = self.body[-1]
        dx, dy = tail.dirnx, tail.dirny

        if dx == 1 and dy == 0:
            self.body.append(Cube((tail.pos[0] - 1, tail.pos[1])))
        if dx == -1 and dy == 0:
            self.body.append(Cube((tail.pos[0] + 1, tail.pos[1])))
        if dx == 0 and dy == 1:
            self.body.append(Cube((tail.pos[0], tail.pos[1] - 1)))
        if dx == 0 and dy == -1:
            self.body.append(Cube((tail.pos[0], tail.pos[1] + 1)))

        self.body[-1].dirnx = dx
        self.body[-1].dirny = dy

    def draw(self, surface):
        for i,c in enumerate(self.body):
            if i == 0:
                c.draw(surface, True)
            else:
                c.draw(surface)


    def isRightDangerous(self):
        global rows
        dx, dy = self.head.dirnx, self.head.dirny
        x, y = self.head.pos[0], self.head.pos[1]

        if dx == 1 and dy == 0:
            if y >= rows - 1:
                return True
            if (x, y + 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == -1 and dy == 0:
            if y <= 0:
                return True
            if (x, y - 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 0 and dy == 1:
            if x <= 0:
                return True
            if (x - 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 0 and dy == -1:
            if x >= rows - 1:
                return True
            if (x + 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        return False

    def isLeftDangerous(self):
        global rows
        dx, dy = self.head.dirnx, self.head.dirny
        x, y = self.head.pos[0], self.head.pos[1]

        if dx == -1 and dy == 0:
            if y >= rows - 1:
                return True
            if (x, y + 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 1 and dy == 0:
            if y <= 0:
                return True
            if (x, y - 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 0 and dy == -1:
            if x <= 0:
                return True
            if (x - 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 0 and dy == 1:
            if x >= rows - 1:
                return True
            if (x + 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        return False

    def isStraightDangerous(self):
        global rows
        dx, dy = self.head.dirnx, self.head.dirny
        x, y = self.head.pos[0], self.head.pos[1]

        if dx == 0 and dy == 1:
            if y >= rows - 1:
                return True
            if (x, y + 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 0 and dy == -1:
            if y <= 0:
                return True
            if (x, y - 1) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == -1 and dy == 0:
            if x <= 0:
                return True
            if (x - 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        if dx == 1 and dy == 0:
            if x >= rows - 1:
                return True
            if (x + 1, y) in list(map(lambda z: z.pos, s.body[1:])):
                return True

        return False

    def isFoodLeft(self, food):
        return self.head.pos[0] > food[0]

    def isFoodRight(self, food):
        return self.head.pos[0] < food[0]

    def isFoodUp(self, food):
        return self.head.pos[1] > food[1]

    def isFoodDown(self, food):
        return self.head.pos[1] < food[1]

    def isMovingLeft(self):
        dx, dy = self.head.dirnx, self.head.dirny

        if dx == -1 and dy == 0:
            return True

        return False

    def isMovingRight(self):
        dx, dy = self.head.dirnx, self.head.dirny

        if dx == 1 and dy == 0:
            return True

        return False

    def isMovingUp(self):
        dx, dy = self.head.dirnx, self.head.dirny

        if dx == 0 and dy == -1:
            return True

        return False

    def isMovingDown(self):
        dx, dy = self.head.dirnx, self.head.dirny

        if dx == 0 and dy == 1:
            return True

        return False

def drawGrid(surface):
    global width, rows
    size = width // rows

    x = 0
    y = 0

    for i in range(rows + 1):
        pygame.draw.line(surface, (255, 255, 255), (x, 0), (x, width))
        pygame.draw.line(surface, (255, 255, 255), (0, y), (width, y))

        x += size
        y += size


def redrawWindow(surface):
    global rows, width, s, score, lastScore, highScore, gameCounter, exploration, exploiting, gameOver, player
    surface.fill((0, 0, 0))
    s.draw(surface)
    snack.draw(surface)
    text = font.render("Score: " + str(score), True, (128, 128, 128))
    text2 = font2.render("Last score: " + str(lastScore), True, (128, 128, 128))
    text3 = font2.render("High score: " + str(highScore), True, (128, 128, 128))
    text4 = font2.render("Round: " + str(gameCounter), True, (128, 128, 128))
    text5 = font2.render("Exploration: " + "{:.2f}".format(exploration) + "%", True, (128, 128, 128))
    text6 = font2.render("Exploiting: " + "{:.2f}".format(exploiting) + "%", True, (128, 128, 128))
    text7 = font2.render(gameOver, True, (128, 128, 128))

    surface.blit(text,(10, 500))
    surface.blit(text2,(10, 570))
    surface.blit(text3,(10, 625))
    surface.blit(text4, (250, 525))
    if learn:
        surface.blit(text5, (250, 575))
        surface.blit(text6, (250, 625))
    surface.blit(text7, (250 - text7.get_width() // 2, 675))

    drawGrid(surface)
    pygame.display.update()

def randomSnack(snake):
    global rows
    positions = snake.body

    while True:
        x = random.randrange(rows)
        y = random.randrange(rows)

        if len(list(filter(lambda z:z.pos == (x,y), positions))) > 0:
            continue
        else:
            break

    return (x,y)


def crash():
    global s, gameCounter, roundActive, rows, learn
    roundActive = False
    agent.give_punishment()
    s.reset(pos=(rows//2, rows//2))
    gameCounter = gameCounter + 1
    if learn:
        agent.replay_new(agent.memory)
    pygame.time.wait(1000)


def eatFood():
    global s, score, snack
    agent.give_reward()
    s.addCube()
    score = score + 1
    snack = Cube(randomSnack(s), color=(0, 255, 0))

def initializeGame(agent):
    agent.replay_new(agent.memory)

def plot_seaborn(counter, array_score):
    counter_array = []
    for c in range(counter):
        counter_array.append(c + 1)
    sns.set(color_codes=True)
    ax = sns.regplot(np.array([counter_array])[0], np.array([array_score])[0], color="b", x_jitter=.1, line_kws={'color':'green'})
    ax.set(xlabel='games', ylabel='score')
    plt.show()

global rows, width, s, snack, score, offset, height, lastScore, highScore, gameCounter, scores, roundActive, exploration, exploiting, gameOver, learn, player

gameOver = ""
exploration = 100
exploiting = 0
score = 0
lastScore = 0
highScore = 0
gameCounter = 1
width = 501
height = 500
offset = 25
rows = 10
scores = []
roundActive = True
player = False

pygame.init()
pygame.font.init()
font = pygame.font.SysFont("comicsansms", 48)
font2 = pygame.font.SysFont("comicsansms", 24)

win = pygame.display.set_mode((width, height + 250))
pygame.display.set_caption("Snake")

s = Snake((255, 0, 0), (rows//2, rows//2))
snack = Cube(randomSnack(s), color = (0, 255, 0))

vel=50
run = True
clock = pygame.time.Clock()
cnt=0

buttonWidth = 250
buttonHeight = 100

agentWithWeights = DQNAgent(False)

while True:
    vel = 50
    if not run:
        pygame.quit()

    learn = False
    player = False
    mainMenu = True

    while mainMenu:
        win.fill((0,0,0))

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()

        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        playPos = (width - buttonWidth) / 2, (height - buttonHeight) / 2
        playDim = (playPos[0], playPos[1], buttonWidth, buttonHeight)
        runPos = (width - buttonWidth)/ 2, (height - buttonHeight) / 2 + 150
        runDim = (runPos[0], runPos[1], buttonWidth, buttonHeight)
        learnPos = (width - buttonWidth)/ 2, (height - buttonHeight) / 2 + 300
        learnDim = (learnPos[0], learnPos[1], buttonWidth, buttonHeight)

        color1 = (0, 0, 50)
        color2 = (0, 0, 100)

        if playPos[0] < mouse[0] < playPos[0] + buttonWidth and playPos[1] < mouse[1] < playPos[1] + buttonHeight:
            pygame.draw.rect(win, color1, playDim)
            if click[0] == 1:
                player = True
                vel = 120
                learn = False
                break
        else:
            pygame.draw.rect(win, color2, playDim)

        if runPos[0] < mouse[0] < runPos[0] + buttonWidth and runPos[1] < mouse[1] < runPos[1] + buttonHeight:
            pygame.draw.rect(win, color1, runDim)
            if click[0] == 1:
                break
        else:
            pygame.draw.rect(win, color2, runDim)

        if learnPos[0] < mouse[0] < learnPos[0] + buttonWidth and learnPos[1] < mouse[1] < learnPos[1] + buttonHeight:
            pygame.draw.rect(win, color1, learnDim)
            if click[0] == 1:
                learn = True
                break
        else:
            pygame.draw.rect(win, color2, learnDim)

        text = font.render("Play", True, (128, 128, 128))
        text2 = font.render("Run Agent", True, (128, 128, 128))
        text3 = font.render("Learn", True, (128, 128, 128))

        win.blit(text, (playPos[0] + 20, playPos[1] + 10))
        win.blit(text2, (runPos[0] + 20, runPos[1] + 10))
        win.blit(text3, (learnPos[0] + 20, learnPos[1] + 10))

        pygame.display.update()
        clock.tick(15)

    if player:
        rounds = 1
    elif learn:
        rounds = 200
    else:
        rounds = 25

    gameCounter = 1
    scores = []
    saveWeights = learn

    if learn:
        agent = DQNAgent(learn)
    else:
        agent = agentWithWeights

    while gameCounter <= rounds and run:
        if learn:
            initializeGame(agent)

        roundActive = True

        agent.epsilon = rounds * 0.2 + 100 - gameCounter

        if not learn:
            agent.epsilon = 0

        if agent.epsilon >= 100:
            exploration = 100
            exploiting = 0
        else:
            if agent.epsilon <= 0:
                exploration = 0
                exploiting = 100
            else:
                exploration = agent.epsilon
                exploiting = 100 - exploration

        while roundActive:
            # get old state
            state_old = agent.get_state(s, snack.pos)

            if player:
                s.movePlayer()
            else:
                # perform random actions based on agent.epsilon, or choose the action
                if randint(0, 100) < agent.epsilon:
                    final_move = to_categorical(randint(0, 2), num_classes=3)
                else:
                    # predict action based on the old state
                    prediction = agent.model.predict(state_old.reshape((1, 11)))
                    final_move = to_categorical(np.argmax(prediction[0]), num_classes=3)

                agent.set_reward(0)
                s.moveAgent(final_move)

            s.move()

            pygame.time.wait(vel)

            state_new = agent.get_state(s, snack.pos)

            if s.body[0].pos == snack.pos:
                eatFood()

            for x in range(len(s.body)):
                if s.body[x].pos in list(map(lambda z: z.pos, s.body[x + 1:])) and roundActive:
                    crash()
                    roundActive = False
                    gameOver = "Round " + str(gameCounter - 1) + " over: Snake hit herself!"
                    break

            redrawWindow(win)

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    saveWeights = False
                    roundActive = False
                    run = False

            reward = agent.reward

            if learn:
                # train short memory base on the new action and state
                agent.train_short_memory(state_old, final_move, reward, state_new, not roundActive)

                # store the new data into a long term memory
                agent.remember(state_old, final_move, reward, state_new, not roundActive)

    # agent.model.save_weights('weights.hdf5')
    if not player:
        plot_seaborn(gameCounter - 1, scores)
